package cz.mendelu.pjj.pohadka.domain;

public class Rytir extends Hrdina {

    int zivoty;
    int sila;

    public Rytir(int zivoty, int sila) {
       this.setZivoty();
       this.setSila();
    }

    public int getZivoty() {
        return zivoty;
    }

    @Override
    public void setSila() {
            this.sila=15;

    }

    @Override
    public void setZivoty() {
            this.zivoty=5;

    }

    public int getSila() {
        return sila;
    }




    @Override
    public void zautoc(Bojeschopny jedinec, int distance) {

        if (jedinec.jeZivy() && distance<=2){
            if (getSila()>jedinec.getSila() || getZivoty()>jedinec.getZivoty()){
                int i=jedinec.getZivoty();
                i--;

            }
            else if(getSila()<jedinec.getSila() || getZivoty()<jedinec.getZivoty()){
                zivoty--;
            }
        }
    }


    @Override
    public boolean jeZivy() {
        if(zivoty==0){
        return false;
        }
        else return true;
    }
}
