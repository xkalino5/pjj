package cz.mendelu.pjj.pohadka;

import bh.greenfoot.runner.GreenfootRunner;
import cz.mendelu.pjj.pohadka.domain.Hranicar;
import cz.mendelu.pjj.pohadka.domain.Rytir;
import cz.mendelu.pjj.pohadka.greenfoot.BattleField;

/**
 * A sample runner for a greenfoot project.
 */
public class PohadkaRunner extends GreenfootRunner {
    static {
        // 2. Bootstrap the runner class.
        bootstrap(PohadkaRunner.class,
                // 3. Prepare the configuration for the runner based on the world class
                Configuration.forWorld(BattleField.class)
                        // Set the project name as you wish
                        .projectName("Rytíři, hraničáři a drak")
                        .hideControls(true)

        );
    }


    public static void main(String[] args) {
        Rytir r=new Rytir(3,12);
        Hranicar h=new Hranicar(1,8);
        Rytir r1=new Rytir(3,10);
        Hranicar h1=new Hranicar(1,8);
        r.zautoc(h,2);
        r.zautoc(r1,1);
        r.zautoc(h1,1);
        r1.zautoc(h1,1);
        r1.zautoc(h,1);
        r1.zautoc(r,1);



        GreenfootRunner.main(args);
    }}