package cz.mendelu.pjj.pohadka.domain;

public abstract class Hrdina implements Bojeschopny{

    public Hrdina() {
    }

    @Override
    public void zautoc(Bojeschopny jedinec, int distance) {

    }

    @Override
    public boolean jeZivy() {
        return false;
    }

    @Override
    public int getSila() {
        return 0;
    }

    @Override
    public int getZivoty() {
        return 0;
    }

    @Override
    public void setSila() {

    }

    @Override
    public void setZivoty() {

    }
}