package cz.mendelu.pjj.pohadka.domain;

public class Hranicar extends Hrdina{

    public Hranicar( int zivoty, int sila){
        super(zivoty,sila);

    }

    @Override
    public void setSila() {
        this.sila=sila;


    }

    @Override
    public void setZivoty() {
        this.zivoty=zivoty;


    }

    public int getZivoty() {
            return zivoty;
        }



    public int getSila() {
            return sila;
        }

}
