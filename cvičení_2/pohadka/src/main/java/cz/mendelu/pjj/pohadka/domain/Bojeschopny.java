package cz.mendelu.pjj.pohadka.domain;

public interface Bojeschopny {

    public int getSila();
    public int getZivoty();
    void setSila();
    void setZivoty();

    void zautoc(Bojeschopny jedinec, int distance);

    boolean jeZivy();

}
