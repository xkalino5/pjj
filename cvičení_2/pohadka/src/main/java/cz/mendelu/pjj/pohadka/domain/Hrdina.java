package cz.mendelu.pjj.pohadka.domain;

public abstract class Hrdina implements Bojeschopny{
    int zivoty;
    int sila;
    public Hrdina(int zivoty,int sila) {
        this.setSila();
        this.setZivoty();
    }

    @Override
    public void zautoc(Bojeschopny jedinec, int distance) {
        if (jedinec.jeZivy() && distance<5){
            if (getSila()>jedinec.getSila() || getZivoty()>jedinec.getZivoty()){
                int i=jedinec.getZivoty();
                i--;
            }
            else if(getSila()<jedinec.getSila() || getZivoty()<jedinec.getZivoty()){
              zivoty--;
            }
        }
    }

    @Override
    public boolean jeZivy() {
        if (zivoty==0){
            return false;}
        else
            return true;
    }


    @Override
    public int getSila() {
        return sila;
    }

    @Override
    public int getZivoty() {
        return zivoty;
    }

    @Override
    public void setSila() {
        this.sila=sila;
    }

    @Override
    public void setZivoty() {
        this.zivoty=zivoty;
    }
}