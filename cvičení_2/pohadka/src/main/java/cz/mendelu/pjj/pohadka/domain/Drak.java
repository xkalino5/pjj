package cz.mendelu.pjj.pohadka.domain;

public class Drak implements Bojeschopny{

    private static Drak smak;

    public static Drak getSmak() {
        if (smak==null){
            smak=new Drak();
        }
        return smak;
    }

    @Override
    public void zautoc(Bojeschopny jedinec, int distance) {

    }

    @Override
    public boolean jeZivy() {
        return false;
    }

    @Override
    public int getSila() {
        return 0;
    }

    @Override
    public int getZivoty() {
        return 0;
    }

    @Override
    public void setSila() {

    }

    @Override
    public void setZivoty() {

    }
}